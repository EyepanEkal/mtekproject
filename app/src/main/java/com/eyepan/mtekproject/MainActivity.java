package com.eyepan.mtekproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {

    ImageView imageMtek;
    ProgressBar pBar;
    Button next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageMtek = findViewById(R.id.ImageMtek);
        next = findViewById(R.id.ProceedBtn);
        pBar = (ProgressBar)findViewById(R.id.p_horizontal);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pBar.setVisibility(ProgressBar.VISIBLE);
                Intent i = new Intent(MainActivity.this,Logscreen.class);
                //doDelay();
                //pBar.setVisibility(ProgressBar.INVISIBLE);
                startActivity(i);
            }
        });

    }

    public void doDelay(){
        int m = 1;
        int y = 199999999;
        while(m<y){
            m++;
        }
        y=1;
        while(y<m){
            y++;
        }
    }
}
