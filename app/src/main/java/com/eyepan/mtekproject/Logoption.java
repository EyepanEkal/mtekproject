package com.eyepan.mtekproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Logoption extends AppCompatActivity {

    ImageView mtek;
    Button fb,twitter,gmail;
    TextView methdLogn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logoption);

        InitElem();
    }

    public void InitElem() {
        mtek = findViewById(R.id.mtekLogo);
        methdLogn = findViewById(R.id.MethdLogin_txt);
        fb = findViewById(R.id.Btn_fb);
        gmail = findViewById(R.id.Btn_gmail);
        twitter = findViewById(R.id.Btn_twitter);

    }

    public void Onclicking(View v){
        switch (v.getId()){
            case R.id.Btn_fb:
                showMessage("Logging in using facebook");
                break;

            case R.id.Btn_gmail :
                showMessage("Logging in using gmail");
                break;

            case R.id.Btn_twitter:
                showMessage("Logging in using twitter");
                break;

            default:
                showMessage("No button pressed");
                break;
        }

        Intent i = new Intent(Logoption.this,Home.class);
        startActivity(i);

    }

    private void showMessage(String text) {
        Toast.makeText(this,text,Toast.LENGTH_SHORT).show();
    }
}
